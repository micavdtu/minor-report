\chapter{Overall Design}
\section{System Overview}

The system can be visualised to have the following components with the interactions as shown:

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{highlevel.png}
\caption{An overall view of the system}
\label{overflow}
\end{figure}

For this project, we have focussed on the execution level, the FMU, and the sensor integration into the FMU. We have also started development on the IP module.



\section{Quad-rotor Dynamic Model}

The quad-rotor has no moving parts and as a result has a simple mechanical design. However, its simplicity in design is challenged by its complicated dynamic system. The quad-rotor will be operating in two frames: the inertial frame and the body frame. The inertial frame is defined by the ground, and gravity points in the negative z direction. The body frame is defined by the orientation of the quad-rotor, with the rotor axes pointing in the positive z direction and the arms pointing in the x and y directions.
%verify about the orientation of x and y axes.

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{body-inertial-frame.png}
\caption{An overall view of the system}
\label{overflow}
\end{figure}

\subsection{Kinematics}

The position and velocity of the quad-rotor in the inertial frame is given by $x = (x, y, z)^T$ and $\dot{x}  = (\dot{x} ,\dot{y} , \dot{z} )^T$, respectively. Similarly, the roll, pitch, and yaw angles in the body frame are given by $\theta =(\phi, \theta, \Psi)^T$ , with corresponding angular velocities given by $ \dot{\Theta} = (\dot{\phi}, \dot{\theta},\dot{\Psi})^T$. Also, the angular velocity vector $\omega = \dot{\Theta}$ is a vector pointing along the axis of rotation. In order to convert these angular velocities into the angular velocity vector, the following relation is used:
\\\\
\begin{equation}
  \omega=
  \begin{bmatrix}
  1 & 0 & -s_\theta \\
  0 & c_\phi & c_\theta s_\phi \\
  0 & -s_\phi & c_\theta c_\phi 
 \end{bmatrix}
 \dot{\theta}
\end{equation}


where $\omega$ is the angular velocity vector in the body frame. The body and inertial frames are related by a rotation matrix R which goes from the body frame to the inertial frame. This matrix is derived by using the ZYZ Euler angle conventions and successively “undoing” the yaw, pitch, and roll.
\\\\
\begin{equation}
R=
\begin{bmatrix}
  c_\phi c_\Psi - c_\theta s_\phi s_\Psi & -c_\Psi s_\phi - c_\phi c_\theta s_\Psi & s_\theta s_\Psi \\
  c_\theta c_\Psi s_\phi + c_\phi s_\Psi & c_\phi c_\theta c_\Psi - s_\phi s_\Psi & -c_\Psi s_\theta \\
  s_\phi s_\theta & c_\phi s_\theta & c_\theta
 \end{bmatrix}
\end{equation}



\subsection{Equations of Motion}

In the inertial frame, the acceleration of the quad-rotor is due to thrust, gravity, and drag. The thrust vector in the inertial frame can be obtained by using the rotation matrix R to map the thrust vector from the body frame to the inertial frame. Thus, the linear motion can be summarized as
\\\\
\begin{equation}
\ddot{x}=
\begin{bmatrix}
  0  \\
  0 \\
 -mg \\ 
 \end{bmatrix}
 + RT_B +F_D
\end{equation}

where $\vec{x}$ is the position of the quad-rotor, g is the acceleration due to gravity, $F_D$ is the drag force, and $T_B$ is the thrust vector in the body frame.$T_B$ and $F_D$ are given by the following matrices:
\\\\
\begin{equation}
T_B=\sum_{i=1}^{4}{T_{i}} = k
\begin{bmatrix}
  0  \\
  0 \\
  \Sigma\omega_i^2 \\ 
 \end{bmatrix}  
 \end{equation}
 
 \begin{equation}
 F_D=
 \begin{bmatrix}
  -k_d \dot{x}  \\
  -k_d \dot{y} \\
  -k_d \dot{z} \\
 \end{bmatrix}
\end{equation}
While it is convenient to have the linear equations of motion in the inertial frame, the rotational equations of motion are useful in the body frame, so that express rotations can be expressed about the center of the quad-rotor instead of about the inertial center. The rotational equations of motion are derived from Euler's equations for rigid body dynamics. Expressed in vector form, Euler's equations are written as
\begin{equation}
I\dot{\omega}  +\omega \times (I\omega) = \tau
\end{equation}

 where $\omega$ is the angular velocity vector, I is the inertia matrix, and $\tau$ is a vector of external torques. This can be rewritten as 
 \begin{equation}
 \dot{\omega}=
 \begin{bmatrix}
  \dot{\omega_x}  \\
  \dot{\omega_y} \\
  \dot{\omega_z} \\ 
  \end{bmatrix}
 =I^\text{-1} (\tau - \omega \times (I\omega))
 \end{equation}
\\\\
We can model our quad-rotor as two thin uniform rods crossed at the origin with a point mass (motor) at the end of each. This results in a simple diagonal inertia matrix of the form.
\begin{equation}
  I=
\begin{bmatrix}	%need both x,y,z to be subscripts%
  I_{xx} & 0 & 0 \\ 
  0 & I_{yy} & 0 \\
  0 & 0 & I_{zz} \\ 
 \end{bmatrix}
\end{equation}

Thus, the final result for the body frame rotational equations of motion are:
\begin{equation}
\dot{\omega}=
\begin{bmatrix}
  \tau_\phi I_{xx}^{-1}  \\
  \tau_\theta I_{yy}^{-1} \\
  \tau_\Phi I_{zz}^{-1} \\ 
 \end{bmatrix}
 -
\begin{bmatrix}
  \frac{I_\text{yy}-I_\text{zz}}{I_\text{xx}} \omega_y \omega_z \\
  \frac{I_\text{zz}-I_\text{xx}}{I_\text{yy}} \omega_x \omega_z \\
  \frac{I_\text{xx}-I_\text{yy}}{I_\text{zz}} \omega_x \omega_y \\ 
 \end{bmatrix}
\end{equation}


\section{Prototype}

Initially, an exploratory prototype was developed that focussed on basic flight dynamics to gain a better understanding of the flight characteristics of the quad-rotor. The initial prototype implemented a basic flight model that dealt with only motor thrusts, yaw, pitch and roll as an open control system. The aim behind the exploratory prototype was to gain a better understanding of the components being used and to test the basic functioning of each of those components. \\\\
The main control equation on this flight model was

\begin{equation}
T_n = T_{in} + K_y \, Y_{in} + K_p \, P_{in} + K_r \, R_{in}
\end{equation}
where, $T_{in}$ is the mapped control input for thrust,
	$Y_{in}$ is the mapped control input for yaw, 
	$K_y$ is the multiplier for yaw input, 
	$P_{in}$ is the mapped control input for pitch, 
	$K_p$ is the multiplier for pitch input, 
	$R_{in}$ is the mapped control input for roll 
and $K_r$ is the multiplier for roll input.

This prototype served as the basic structure for our current iteration of the flight management unit.   

\section{Control System Design}

We intended to implement a linear time invariant system for the quad-rotor platform that would be running on the Arduino microcontroller. The general equations of an LTI are:

\begin{equation}
\dot{x} = A\,x + B\,u
\label{CS1}
\end{equation}

\begin{equation}
y = C\,x+D\,u
\label{CS2}
\end{equation}

where, A matrix is a 12$\times$12 matrix. The matrix entries are governed by the physical forces that govern the quad-rotor motion. The B matrix is a 12$\times$4 matrix whose entries are governed by the actuators we have in our system. The C matrix is a 4$\times$12 matrix whose entries are governed by what sensors we have in our system. The D matrix is a 4$\times$4 order matrix. The $x$ matrix is a 12$\times$1 order matrix and it contains the states of the system. The $u$ matrix is a 4$\times$1 order matrix and it contains the inputs to the system. The $y$ matrix is a 4$\times$1 order matrix and it contains the outputs of the system.

\setcounter{MaxMatrixCols}{30}

\begin{equation}
A=
\begin{bmatrix}
0 & 0 & 0 & 0 & 0 & 0 & 0 	& 9.81 	& 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & -9.81 & 0 	& 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 	& 0 		& 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 	& 0 		& 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 	& 0 		& 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 	& 0 		& 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 1 & 0 & 0 & 0 	& 0		& 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 1 & 0 & 0 	& 0 		& 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 1 & 0 	& 0 		& 0 & 0 & 0 & 0 \\
1 & 0 & 0 & 0 & 0 & 0 & 0 	& 0 		& 0 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 0 & 0 & 0 	& 0 		& 0 & 0 & 0 & 0 \\
0 & 0 & 1 & 0 & 0 & 0 & 0 	& 0 		& 0 & 0 & 0 & 0 

\end{bmatrix}
\end{equation}

\begin{equation}
B=
\begin{bmatrix}
0 & 0 & 0 & 0  \\
0 & 0 & 0 & 0 \\
k_1 & k_2 & k_3 & k_4 \\
k_5 & k_6 & k_7 & k_8 \\
k_{9} & k_{10} & k_{11} & k_{12} \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0
\end{bmatrix}
\end{equation}

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{statespace.png}
\caption{Control system statespace}
\label{overflow}
\end{figure}


%\section{Control System Tuning}



\section{PID Design}

Proportional-Integral-Derivative (PID) control is the most common control algorithm used in industry and has been universally accepted in industrial control. The popularity of PID controllers can be attributed partly to their robust performance in a wide range of operating conditions and partly to their functional simplicity, which allows engineers to operate them in a simple, straightforward manner. As the name suggests, PID algorithm consists of three basic coefficients; proportional, integral and derivative which are varied to get optimal response. Closed loop systems are the primary application area of PID controllers. The PID controller can be described by the function:

\begin{equation}
u(t) =  K_P\,error(t) + K_I \int \,error(t)\text{d}t + K_D \frac{\text{d}}{\text{d}t}\,error(t).
\end{equation}

The basic idea behind a PID controller is to read a sensor, then compute the desired actuator output by calculating proportional, integral, and derivative responses and summing those three components to compute the output. These responses are calculated based on an error term:

\begin{equation}
error = setpoint_{desired} - setpoint_{actual}
\end{equation}

\subsection{Proportional Response}
The proportional component depends only on the difference between the set point and the process variable or the $error$. The proportional gain ($K_P$) determines the ratio of output response to the error signal. For instance, if the error term has a magnitude of 10, a proportional gain of 5 would produce a proportional response of 50. In general, increasing the proportional gain will increase the speed of the control system response. However, if the proportional gain is too large, the process variable will begin to oscillate. If Kc is increased further, the oscillations will become larger and the system will become unstable and may even oscillate out of control.

\begin{equation}
Proportional\,Term = K_P\,e(t)
\end{equation}

\subsection{Integral Response}
The integral component sums the error term over time. The result is that even a small error term will cause the integral component to increase slowly. The integral response will continually increase over time unless the error is zero, so the effect is to drive the Steady-State error to zero. Steady-State error is the final difference between the process variable and set point.

\begin{equation}
Integral\,Term = K_I \int e(t)\text{d}t
\end{equation}

\subsection{Derivative Response}
The derivative component causes the output to decrease if the process variable is increasing rapidly. The derivative response is proportional to the rate of change of the process variable. Increasing the derivative time ($T_d$) parameter will cause the control system to react more strongly to changes in the error term and will increase the speed of the overall control system response. Most practical control systems use very small derivative time ($T_d$), because the Derivative Response is highly sensitive to noise in the process variable signal. If the sensor feedback signal is noisy or if the control loop rate is too slow, the derivative response can make the control system unstable.

\begin{equation}
Derivative\,Term = K_D \frac{\text{d}}{\text{d}t}e(t)
\end{equation}


\section{PID Tuning}
The process of setting the optimal gains for P, I and D to get an ideal response from a control system is called tuning. There are different methods of tuning of which the manual method and the Ziegler Nichols method will be used.

\subsection{Manual Method}
The gains of a PID controller can be obtained by trial and error. In this method, the I and D terms are set to zero and the proportional gain is increased until the output of the loop oscillates. As one increases the proportional gain, the system becomes faster, but care must be taken not make the system unstable. Once P has been set to obtain a desired fast response, the integral term is increased to stop the oscillations. The integral term reduces the steady state error, but increases overshoot. Some amount of overshoot is always necessary for a fast system so that it could respond to changes immediately. The integral term is tweaked to achieve a minimal steady state error. Once the P and I have been set to get the desired fast control system with minimal steady state error, the derivative term is increased until the loop is acceptably quick to its set point. Increasing derivative term decreases overshoot and yields higher gain with stability but would cause the system to be highly sensitive to noise. 

\subsection{Ziegler Nicholas Method}
The Ziegler-Nichols method is another popular method of tuning a PID controller. It is very similar to the manual method wherein I and D are set to zero and P is increased until the loop starts to oscillate. Once oscillation starts, the critical gain Kc and the period of oscillations Pc are noted. The P, I and D are then adjusted as per the tabular column shown below.

\begin{table}{}
\caption{Ziegler Nichols Tuning Constants}
\begin{center}
\begin{tabular}{| l | c | c | c |}
Control Type	 & $K_p$			& 	$K_i$ 				& 	$K_d$ \\
P			 & $0.50(K_u)$ 	& 	  - 					& 		-\\
PI			 & $0.45(K_u)$ 	&	$1.2(K_p)/P_u$		& 		-\\
PID			 & $0.60(K_u)$	& 	$2(K_p)/P_u$ 		&	$(K_p)(P_u)/8$ \\
\end{tabular}

\end{center}

\end{table}



\section{DSP Design and Testing}

We have considered filters in the time domain for signal smoothing i.e. the simple moving average (SMA), cumulative moving average (CMA), exponential moving average (EMA), the Savitzky-Golay filter, and the Ramer-Douglas-Peucker (RDP) algorithm. 

\subsection{Simple Moving Average}
The SMA is the mean of the set of data points distributed uniformly on either side of a central value. The computed mean is then set as the control value in place of the received value at that time instant. The number of data points, also known as the window size can be varied. 

\subsection{Cumulative Moving Average}
The CMA takes the mean of all the data that has arrived till that particular instant. It is given by the formula:

\begin{equation}
C_{k+1} = \dfrac{x_{k+1}+xC_{k}}{k+1} 
\end{equation}
\begin{equation}
C_{k} = \dfrac{\sum_{i=1}^{k}{x_{i}}}{k}
\end{equation}

where $C_{k}$ is the cumulative average of $k$ data points and $x_{k}$ is the value of $k^{th}$ data point. $C_{0}$ is taken as zero.
Similar to SMA, we again compute the CMA and set it as the control value, in place of the received value at that time instant.

\subsection{Exponential Moving Average}
The EMA is a type of weighted moving average with an exponential weighting function \cite{EMA}. The weighting function uses a continuously decreasing exponential function. A constant factor, alpha, represents the degree of decrease in successive weights, and lies between 0 and 1. A smaller value of alpha takes more of the previous readings into account for calculating the current signal value. The EMA for a series S can be calculated as:

\begin{align*}
S_{1} = C_{1} \quad{\text{for}} \;\; t=1
\end{align*}
\begin{equation}
S_t = \alpha Y_t + (1-\alpha)S_{t-1} \quad{\text{for}} \;\; t>1
\end{equation}

where $Y_t$ is the data value at a time instant $t$. $S_t$ is the value of the EMA at any time instant $t$.

\begin{figure}[!t]
\centering
\includegraphics[width=90mm]{sampleplot}
\caption{Plot representing input signal and smoothed output for EMA 0.10}
\label{fig:sampleplot}
\end{figure}

\subsection{Savitzky Golay Filter}
The SG filter is a digital filter, which works on the method of fitting least squares, of a polynomial of a given order, to data points in a moving window. The polynomial is evaluated at the center of the moving window, and that value is the filtered value. The window is shifted over the entire data set and the central value of the window is calculated in each iteration of the algorithm. The degree of the polynomial used for least squares fitting and the moving window size can be varied and tested to obtain the most appropriate combination of parameters. The convolution coefficients for smoothing the signal can be obtained from the original paper of Savitzky and Golay \cite{SG}.

\subsection{Ramer Douglas Peucker Algorithm}
The final algorithm implemented was RDP algorithm \cite{RDP}. Given a set of curves, it attempts to reduce the number of points required to best approximate the curve. The algorithm uses the perpendicular distance of points from the curve to estimate which points need to be ignored. The ones at a distance greater than a fixed value ($\epsilon$) are considered significant and remain part of the final smoothed output. It can be applied for smoothing noisy signals by filtering the data set to include fewer points. 

\subsection{Testing}
Now that the algorithms were implemented, we executed the program on the Arduino board and observed the performance in real time. An open serial connection between the Arduino board and the workstation was used to collect data for a period of 30 seconds using the input signal as described above. Each record of the data set collected consisted of four parameters: time of call of smoothing function, raw value of input signal, time of return of smoothing function, and the smoothed signal value. We then analyzed the data and evaluated the performance of each algorithm.

\section{Image Processing}
The main aim of using image processing in this project is to identify the digits being displayed on a screen. The overall design of the image processing module is as follows:
%image
\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{img_proc_chart.png}
\caption{An overall view of the system}
\label{overflow}
\end{figure}

Firstly, we were required to capture the images from the camera and send it to the RPI. As the Open CV was installed on RPI, image processing was performed on RPI. Now we need to identify the digits in the captured images. This is basically a two step process:

\begin{enumerate}
\item Screen Localization: Extract the screen from the image.
\item Digits Identification: Extract the digits on the screen and then identify them.
\end{enumerate}

For extracting the screen from the image i.e. the localization of the screen, SVM algorithm was used. Whereas for the second step, two algorithms k-nearest algorithm and artificial neural intelligence were applied and compared.

\subsection{Screen Localization} 
Localizing the screen in the captured image itself is a two step process:
\begin{enumerate}
\item Segmentation: We apply different filters, morphological operations, contour algorithms, and validations to retrieve those parts of the image that could have a screen.
\item Classification: We apply a Support Vector Machine (SVM) classifier to each image patch—our feature. Before creating our main application, we train with two different classes: screen and non-screen images which acts as our training data.
\end{enumerate}

%\subsubsection{Segmentation}
Segmentation is a process of dividing the image into several segments and then finding and extracting the regions of our interest. One important feature of our image will be that it will be having lots of vertical lines. We can use this as out tool to segment the image. 
First, we convert our image to a gray scale image and then apply a sobel filter to extract vertical edges. Then we apply a threshold filter to obtain a binary image. After this we apply a close morphological operation in order to remove blank spaces between each vertical edge and connect all vertical lines that have a high number of edges. After this step we have regions in our image that could possibly have a screen. Then we find the external contours in the image and for each contour detected, extract the bounding rectangle of minimal area. We then make basic validations on its area and aspect ratio so as to classify them in a screen or a not screen region %\subsubsection{Classification}
After we preprocess and segment all possible parts of an image, we now need to decide if each segment is (or is not) a screen. To do this, we will use a Support Vector Machine (SVM) algorithm. %\subsubsubsection{•What is SVM}
%\subsubsection{What is SVM?}
A Support Vector Machine (SVM) is a discriminative classifier formally defined by a separating hyperplane. In other words, given labelled training data (supervised learning), the algorithm outputs an optimal hyperplane which categorizes new examples. 
%For example: for a linearly separable set of 2D-points which belong to one of two classes, we need to find a line that could separate them. 
%image -svm.png   svm_1.png
%from the above figure we can see that there are several such lines but we need to find such line which provides a minimum distance from the training examples.

One of the basic requirements of SVM is that it requires a lot of training data in order to classify the image. We trained our system with several screen images and several non-screen images. SVM classifier uses these images to train the system and saves the resultant information in an XML file. Finally it uses the information stored in an XML file in order to use them to detect screen in the test data.

\subsection{Digits Identification}
The second step aims to retrieve the characters on the screen with optical character recognition. For the detected screen, we proceed to find the position of digit in that image and then apply Artificial Neural Network (ANN) machine-learning algorithm to recognize the character.

ANN is more particularly a multi-layer perceptrons (MLP), the most commonly used type of neural networks. MLP consists of the input layer, output layer, and one or more hidden layers. Each layer of MLP includes one or more neurons directionally linked with the neurons from the previous and the next layer. Figure \ref{multi-layer} represents a 3-layer perceptron with three inputs, two outputs, and the hidden layer including five neurons.

\begin{figure}[ht!]
\centering    
\includegraphics[width=70mm,height=40mm]{ann.png}
\caption{multi-layer perceptrons (MLP)}
\label{multi-layer}
\end{figure}

All the neurons in MLP are similar. Each of them has several input links (it takes the output values from several neurons in the previous layer as input) and several output links (it passes the response to several neurons in the next layer). The values retrieved from the previous layer are summed up with certain weights, individual for each neuron, plus the bias term. The sum is transformed using the activation function $f$ that may be also different for different neurons.

\begin{figure}[ht!]
\centering
\includegraphics[width=70mm,height=40mm]{ann_model.png}
\caption{model of neural networks}
\label{overflow}
\end{figure}

The basic design of the second step is as shown in figure \ref{ann-flow}. 

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{ann_flowchart.png}
\caption{multi-layer perceptrons (basic design)}
\label{ann-flow}
\end{figure}

\begin{enumerate}

	\item Dataset
	The data set used consists of a training set of 3050 samples ( 305 samples per digit).

	\item Pre-Processing module
	The pre-processing  module is responsible for converting our input data into a format, which is required by the 
	neural network. The diagram \ref{pre-proc} gives an overview of what happens to each image in the dataset.

	\begin{enumerate}
	\item Once the image is loaded, we apply GaussianBlur to remove noise and then apply thresholding to obtain
	a binary image.
	\item Crop the image to eliminate the extra white space. 
	\item Reduce the image dimension to 16×16. (16 rows with 16 pixels each).
	\item Write the value of pixels into a file followed by the label, one image per row. 
	For e.g. Consider the above image of 2. Each black pixel will be represented by 0 and white pixel by 1.So in
	the file we will write something like 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, ……0, 0, 0, 0, 0, 0, 0, 	0, 0, 0, 0, 0, 0, 0, 0, 0, 2
	\end{enumerate}

	\item Training Module
	The training module is responsible for taking our transformed training set and generate the model of Neural
	Network.

	\item Classification module
	This module will use the ANN model generated by the Training module to classify the user input.

\end{enumerate}


\begin{figure}[ht!]
\centering
\includegraphics[width=90mm,height=60mm]{preprocessing.png}
\caption{Pre-processing step}
\label{pre-proc}
\end{figure}